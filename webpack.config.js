var path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: ['@babel/polyfill', path.resolve(__dirname, 'src') + '/index.js'],
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: '/'
    },
    module: {
      rules: [
        {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env', '@babel/preset-react']
              }
            }
          },
          {
            test: /\.(scss|css)$/,
            use: [{
                loader: 'style-loader'
            }, {
                loader: 'css-loader'
            }, {
                loader: 'sass-loader'
            }]
          },
          {
            test: /\.(png|svg|ttf|eot|woff|woff2)$/,
            use: ['url-loader']
          }
      ]
    }
};
